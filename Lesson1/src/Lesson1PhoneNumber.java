/*
* Main
*
* Version info
*
* Dmytryi_Bezverhyi
*
* Home work GeekHUB java4web_group1
* This program calculation sum of numbers that make up
* the previous sum. The first input must be a phone number.
* Based on string and operation whit String and char.
 */

import java.util.Scanner;

public class Lesson1PhoneNumber {

    private static String[] CODE = {"039", "050", "063", "066", "067", "068",
            "091", "092", "093", "094", "095", "096", "097", "098", "099"};

    private static boolean numberCheck(String n){
        String codeN;

        //Check a length of input and parse a number to code and number
        switch(n.length()){
            case 13:
                if (n.charAt(0) == '+' && n.charAt(1) == '3' && n.charAt(2) == '8') {
                    codeN = n.substring(3, 6);
                    n = n.substring(6);
                } else {
                    return false;
                }
                break;
            case 11:
                if (n.charAt(0) == '8') {
                    codeN = n.substring(1, 4);
                    n = n.substring(4);
                } else {
                    return false;
                }
                break;
            case 10:
                codeN = n.substring(0, 3);
                n = n.substring(3);
                break;
            default:
                return false;
        }

        for (int i = 0; i < 7; i++) { //Check a phone number
            char temp = n.charAt(i);
            if (!(temp >= '0' && temp <= '9')) {
                return false;
            }
        }

        for (String code : CODE) { //Check a phone code
            if (codeN.contentEquals(code)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String input;
        int attempt = 0;
        System.out.println("Calculating sum of phone number, support format:"
                + "\n+38xxx1234567\n8xxx1234567\nxxx1234567\nfor quit enter q");
        System.out.print("Please enter phone number: ");

        while (true) {
            input = in.next();

            if ((input.charAt(0) == 'q') && (input.length() == 1)) {
                System.out.println("Quit!");
                break;
            } else {
                if (numberCheck(input)) {
                    int iter = 1;
                    int sum;
                    String number = input.substring(input.length() - 10);
                    System.out.println("\n" + number + " number is correct!");

                    do {
                        sum = 0;
                        System.out.print(iter + "st round of calculation, sum is:");
                        for (int i = 0, l = number.length(); i < l; i++) {
                            sum = sum + (number.charAt(i) - 48);
                        }
                        number = Integer.toString(sum);
                        System.out.println(number);
                        iter++;
                    } while (number.length() > 1);

                    switch(sum){
                        case 1:
                            System.out.println("Final result is: One" );
                            break;
                        case 2:
                            System.out.println("Final result is: Two" );
                            break;
                        case 3:
                            System.out.println("Final result is: Three" );
                            break;
                        case 4:
                            System.out.println("Final result is: Four" );
                            break;
                        default:
                            System.out.println("Final result is: " + sum );
                    }
                    break;
                } else {
                    if (attempt < 3) {
                        System.out.println("Sorry, number is incorrect!");
                        System.out.print("Please enter phone number: ");
                    } else {
                        System.out.println("Sorry, number is incorrect!\nSupport format:"
                                + "\n+38xxx1234567\n8xxx1234567\nxxx1234567\n");
                        System.out.print("Please enter phone number: ");
                        attempt = 0;
                    }
                    attempt++;
                }
            }
        }
    }
}

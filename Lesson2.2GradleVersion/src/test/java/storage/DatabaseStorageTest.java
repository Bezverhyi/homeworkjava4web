package storage;

import first.objects.Cat;
import first.storage.DatabaseStorage;
import org.junit.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;
import static org.junit.Assert.*;

public class DatabaseStorageTest {
    private static Connection connection;
    private static DatabaseStorage storage;

    @BeforeClass
    public static void openConnection() throws Exception {
        connection = DriverManager.getConnection("jdbc:h2:~/testHomework2.2", "sa", "");
        storage = new DatabaseStorage(connection);
    }

    @AfterClass
    public static void closeConnection() throws Exception {
        connection.close();
    }

    @Before
    public void createTables() throws Exception {
        String createCatTableStatement = "Create table if not exists cat (\n" +
                "id int not null auto_increment primary key,\n" +
                "name varchar(45),\n" +
                "age int)";
        Statement sqlStatement = connection.createStatement();
        sqlStatement.execute(createCatTableStatement);
    }

    @After
    public void dropTables() throws Exception {
        Statement sqlStatement = connection.createStatement();
        sqlStatement.execute("DROP TABLE cat");
    }

    @Test
    public void testGetCorrect() throws Exception {
        Statement sqlStatement = connection.createStatement();
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('Red', 2)");
        Cat cat = storage.get(Cat.class, 1);

        assertEquals(2, cat.getAge());
        assertEquals("Red", cat.getName());
    }

    @Test
    public void testGetEmpty() throws Exception {
        assertEquals(null, storage.get(Cat.class, 1));
    }

    @Test
    public void testListNumberOfData() throws Exception {
        Cat red = new Cat();
        red.setAge(2);
        red.setName("Red");
        Cat green = new Cat();
        green.setAge(3);
        green.setName("Green");

        Statement sqlStatement = connection.createStatement();
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('Red', 2)");
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('Green', 3)");

        List<Cat> entitiesList = storage.list(Cat.class);

        assertEquals("Check number of entities in list", 2, entitiesList.size());
        assertTrue("Check is list contain red cat", entitiesList.contains(red));
        assertTrue("Check is list contain green cat", entitiesList.contains(green));
    }

    @Test
    public void testListEmptyDataBase() throws Exception {
        List<Cat> entitiesList = storage.list(Cat.class);
        assertEquals(0, entitiesList.size());
    }

    @Test
    public void testDeleteCheckNumberOfDeletedEntities() throws Exception {
        Statement sqlStatement = connection.createStatement();
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('Red', 2)");
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('Green', 3)");
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('White', 1)");

        assertEquals("Delete entities when entities exist", 3, storage.delete(Cat.class));
        assertEquals("Delete entities when entities not exist", 0, storage.delete(Cat.class));
    }

    @Test
    public void testSaveNewObject() throws Exception {
        Cat red = new Cat();
        red.setAge(2);
        red.setName("Red");

        storage.save(red);
        assertEquals(red, storage.get(Cat.class, 1));
    }

    @Test
    public void testSaveCheckUpdateObjectInDatabase() throws Exception {
        Cat red = new Cat();
        red.setAge(2);
        red.setName("Red");

        storage.save(red);

        Cat green = new Cat();
        green.setAge(3);
        green.setName("Green");
        green.setId(1);

        storage.save(green);

        assertEquals(green, storage.get(Cat.class, 1));
    }

    @Test
    public void testBooleanDelete() throws Exception {
        Statement sqlStatement = connection.createStatement();
        sqlStatement.execute("INSERT INTO cat (name, age) VALUES ('Red', 2)");
        Cat savedCat = storage.get(Cat.class, 1);
        assertTrue(storage.delete(savedCat));
        Cat deletedCat = storage.get(Cat.class, 1);
        assertEquals(null, deletedCat);
    }
}
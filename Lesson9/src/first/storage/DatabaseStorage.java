package first.storage;

import first.objects.Entity;
import first.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {

    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws StorageException {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws StorageException {
        String sqlStatement = "SELECT * FROM " + clazz.getSimpleName();

        try (PreparedStatement preparedSqlStatement = connection.prepareStatement(sqlStatement)) {
            return extractResult(clazz, preparedSqlStatement.executeQuery());
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws StorageException {
        String sqlStatement = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = ?" ;

        try (PreparedStatement preparedSqlStatement = connection.prepareStatement(sqlStatement);) {
            preparedSqlStatement.setObject(1, entity.getId());

            return preparedSqlStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws StorageException {
        try {
            Map<String, Object> entityMap = prepareEntity(entity);
            String sqlStatement;

            if (entity.isNew()) {
                sqlStatement = prepareInsertStatement(entity, entityMap);
            } else {
                sqlStatement = prepareUpdateStatement(entity, entityMap);
            }

            executeSaveStatement(entity, entityMap, sqlStatement);
        } catch (IllegalAccessException e) {
            throw new StorageException(e);
        }
    }

    private <T extends Entity> String prepareInsertStatement(T entity, Map<String, Object> entityMap) {
        String sqlStatement = "INSERT INTO " + entity.getClass().getSimpleName();
        String sqlKeys = "";
        String sqlValues = "";

        for (String key : entityMap.keySet()) {
            sqlKeys = sqlKeys + key + ",";
            sqlValues = sqlValues + "?,";
        }

        return sqlStatement = sqlStatement + " (" + sqlKeys.substring(0, sqlKeys.length() - 1) + ") "
                + "VALUES" + " (" + sqlValues.substring(0, sqlValues.length() - 1) + ")";
    }

    private <T extends Entity> String prepareUpdateStatement(T entity, Map<String, Object> entityMap) {
        String sqlStatement = "UPDATE " + entity.getClass().getSimpleName() + " SET ";
        String sqlUpdate = "";

        for (String key : entityMap.keySet()) {
            sqlUpdate = sqlUpdate + key + "=?,";
        }

        return sqlStatement = sqlStatement + sqlUpdate.substring(0, sqlUpdate.length() - 1) + " WHERE " + "id =" + entity.getId();
    }

    private <T extends Entity> void executeSaveStatement(T entity, Map<String, Object> entityMap, String sqlStatement) throws StorageException {
        try (PreparedStatement preparedSqlStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS)) {
            String[] keys = entityMap.keySet().toArray(new String[0]);

            for (int i = 0; i < keys.length; i++) {
                preparedSqlStatement.setObject(i + 1, entityMap.get(keys[i]));
            }

            preparedSqlStatement.executeUpdate();
            ResultSet resultSet = preparedSqlStatement.getGeneratedKeys();

            if (resultSet.next()) {
                entity.setId(resultSet.getInt(1));
            }

        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws IllegalAccessException {
        Map<String, Object> entityMap = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();

        for (Field field : fields) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                field.setAccessible(true);
                entityMap.put(field.getName(), field.get(entity));
            }
        }

        return entityMap;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> resultList = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();

        while (resultSet.next()) {
            T resultObject = clazz.newInstance();
            resultObject.setId(resultSet.getInt("id"));

            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    field.set(resultObject, resultSet.getObject(field.getName()));
                }
            }

            resultList.add(resultObject);
        }

        return resultList;
    }
}

package third;

import third.objects.Employee;
import third.objects.Salary;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;

public class Main {
    public static final String DB_URL = "jdbc:h2:";
    public static final String DB_NAME = "~/homework9.3";
    public static final String LOGIN = "sa";
    public static final String PASSWORD = "";

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection(DB_URL + DB_NAME, LOGIN, PASSWORD)) {
            Statement statement = connection.createStatement();
            statement.execute(StatementStorage.createEmployeeTableSatement);
            statement.execute(StatementStorage.createSalaryTableStatement);

            Employee[] employees = {new Employee("Joe"),
                                    new Employee("David"),
                                    new Employee("Adam")};

            Salary[] salaries = new Salary[15];

            for (int i = 0; i < salaries.length; i++) {
                salaries[i] = new Salary(new BigDecimal(Math.random() * 1000), employees[i % employees.length], new Date());
            }

            try (PreparedStatement preparedSqlStatement
                         = connection.prepareStatement(StatementStorage.sqlStatementInsertEmployee,
                                                       Statement.RETURN_GENERATED_KEYS)) {
                for (Employee employee : employees) {
                    preparedSqlStatement.setObject(1, employee.getName());

                    preparedSqlStatement.executeUpdate();

                    ResultSet id = preparedSqlStatement.getGeneratedKeys();
                    if (id.next()) {
                        employee.setId(id.getInt(1));
                    }
                }
            }

            try (PreparedStatement preparedSqlStatement
                         = connection.prepareStatement(StatementStorage.sqlStatementInsertSalary,
                                                       Statement.RETURN_GENERATED_KEYS)) {
                for (Salary salary : salaries) {
                    preparedSqlStatement.setObject(1, salary.getValue());
                    preparedSqlStatement.setObject(2, salary.getEmployee().getId());
                    preparedSqlStatement.setObject(3, salary.getDate());

                    preparedSqlStatement.executeUpdate();

                    ResultSet idAndDate = preparedSqlStatement.getGeneratedKeys();
                    if (idAndDate.next()) {
                        salary.setId(idAndDate.getInt(1));
                    }
                }
            }

            ResultSet selectResult = statement.executeQuery(StatementStorage.selectStatementSalary);

            while (selectResult.next()) {
                System.out.print(selectResult.getObject("id"));
                System.out.print("   ");
                System.out.print(selectResult.getObject("name"));
                System.out.print("   ");
                System.out.println(selectResult.getObject("salary.value"));
            }

            ResultSet selectSumResult = statement.executeQuery(StatementStorage.selectStatementTotalSalary);

            System.out.println("Total sum: ");

            while (selectSumResult.next()) {
                System.out.print(selectSumResult.getObject("id"));
                System.out.print("   ");
                System.out.print(selectSumResult.getObject("name"));
                System.out.print("   ");
                System.out.println(selectSumResult.getObject("sum"));
            }

            statement.execute(StatementStorage.dropEmployeeTableStatement);
            statement.execute(StatementStorage.dropSalaryTableStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

/*
EXAMPLE OF CONSOLE OUTPUT
1   Joe   550.56
2   David   860.09
3   Adam   508.13
1   Joe   845.59
2   David   241.18
3   Adam   128.28
1   Joe   270.30
2   David   554.09
3   Adam   303.64
1   Joe   615.61
2   David   58.66
3   Adam   424.66
1   Joe   307.76
2   David   676.52
3   Adam   135.94
Total sum:
1   Joe   2589.82
2   David   2390.54
3   Adam   1500.65
 */
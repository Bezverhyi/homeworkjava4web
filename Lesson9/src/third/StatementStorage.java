package third;

/**
 * Created by Dmytryi on 18.12.2016.
 */
public class StatementStorage {
    public static final String createEmployeeTableSatement = "CREATE TABLE IF NOT EXISTS employee (" +
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
            "name VARCHAR(45) NOT NULL)";

    public static final String createSalaryTableStatement = "CREATE TABLE IF NOT EXISTS salary (\n" +
            "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
            "date TIMESTAMP NOT NULL DEFAULT NOW(),\n" +
            "value DECIMAL(15, 2) NOT NULL DEFAULT 0.0,\n" +
            "emp_id INT,\n" +
            "CONSTRAINT salary_emloyee_id FOREIGN KEY (emp_id) " +
            "REFERENCES employee (id) ON DELETE CASCADE ON UPDATE CASCADE)";

    public static final String dropEmployeeTableStatement = "DROP TABLE employee";
    public static final String dropSalaryTableStatement = "DROP TABLE salary";

    public static final String sqlStatementInsertEmployee = "INSERT INTO employee (name) VALUES (?)";
    public static final String sqlStatementInsertSalary = "INSERT INTO salary (value, emp_id, date) VALUES (?, ?, ?)";

    public static final String selectStatementSalary = "SELECT employee.id, employee.name, salary.value " +
            "FROM salary " +
            "INNER JOIN employee " +
            "ON salary.emp_id = employee.id ";
    public static final String selectStatementTotalSalary = "SELECT employee.id, employee.name, SUM(salary.value) AS sum " +
            "FROM salary " +
            "INNER JOIN employee " +
            "ON salary.emp_id = employee.id " +
            "GROUP BY employee.id";
}

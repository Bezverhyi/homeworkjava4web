package third.objects;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Dmytryi on 18.12.2016.
 */
public class Salary {
    private int id;
    private Employee employee;
    private BigDecimal value;
    private Date date;

    public Salary() {
    }

    public Salary(BigDecimal value, Employee employee, Date date) {
        this.value = value;
        this.employee = employee;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "id=" + id +
                ", employee=" + employee +
                ", value=" + value +
                ", date=" + date +
                '}';
    }
}


package second.car;

public class Car implements Comparable<Car>{
    private String name;
    private int price;

    public Car(String name, int price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public int compareTo(Car car) {
        if (price == car.getPrice() && name.equals(car.getName())) {
            return 0;
        } else {
            for (int i = 0; i < 3; i++) {
                if (name.charAt(i) < car.getName().charAt(i)) {
                    return -1;
                } else if (name.charAt(i) > car.getName().charAt(i)) {
                    return 1;
                }
            }
            return price - car.getPrice();
        }
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "name: " + name + " price: " + price;
    }
}

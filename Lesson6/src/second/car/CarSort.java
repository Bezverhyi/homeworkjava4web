package second.car;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CarSort {

    public static void main(String[] args) {
        Car[] cars = {new Car("Ford", 135),
                      new Car("Volkswagen", 100),
                      new Car("Volkswagen", 120),
                      new Car("Ford", 130),
                      new Car("Volkswagen", 110),
                      new Car("Fiat", 90),
                      new Car("Ford", 115),
                      new Car("Mercedes", 130)};

        List<Car> carsList = Arrays.asList(cars);

        Collections.sort(carsList, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.compareTo(o2);
            }
        });
    }
}

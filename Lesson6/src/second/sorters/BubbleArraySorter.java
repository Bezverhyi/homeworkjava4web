package second.sorters;

public class BubbleArraySorter implements ArraySorter {
    @Override
    public Comparable[] sort(Comparable[] elements, Direction direction) {
        boolean isSorted;

        for (int i = elements.length - 1; i > 0; i--) {
            isSorted = true;

            for (int j = 0; j < i; j++) {
                if ((elements[j].compareTo(elements[j + 1]) * direction.getDirection()) > 0) {
                    isSorted = false;
                    swap(elements, j);
                }
            }

            if (isSorted) {
                break;
            }
        }

        return elements;
    }

    private void swap(Comparable[] elements, int index) {
        Comparable temp = elements[index];
        elements[index] = elements[index + 1];
        elements[index + 1] = temp;
    }
}

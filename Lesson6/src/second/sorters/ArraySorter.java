package second.sorters;

public interface ArraySorter {

    Comparable[] sort(Comparable[] elements, Direction direction);
}
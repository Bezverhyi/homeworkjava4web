package second.sorters;

public class InsertionArraySorter implements ArraySorter {
    @Override
    public Comparable[] sort(Comparable[] elements, Direction direction) {
        int minIndex;

        for (int i = 1; i < elements.length; i++) {
            for (int j = i; j > 0; j--) {
                if ((elements[j - 1].compareTo(elements[j]) * direction.getDirection()) > 0) {
                    swap(elements, j, j - 1);
                } else {
                    break;
                }
            }
        }
        return elements;
    }

    private void swap(Comparable[] elements, int index, int indexMin) {
        Comparable temp = elements[index];
        elements[index] = elements[indexMin];
        elements[indexMin] = temp;
    }
}

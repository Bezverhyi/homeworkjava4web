package second.sorters;

public class SelectionArraySorter implements ArraySorter {
    @Override
    public Comparable[] sort(Comparable[] elements, Direction direction) {
        int minElementIndex;

        for (int i = 0; i < elements.length; i++) {
            minElementIndex = i;

            for (int j = i + 1; j < elements.length; j++) {
                if ((elements[minElementIndex].compareTo(elements[j]) * direction.getDirection()) > 0) {
                    minElementIndex = j;
                }
            }
            if (i != minElementIndex) {
                swap(elements, i, minElementIndex);
            }
        }
        return elements;
    }

    private void swap(Comparable[] elements, int index, int minElementIndex) {
        Comparable temp = elements[index];
        elements[index] = elements[minElementIndex];
        elements[minElementIndex] = temp;
    }
}

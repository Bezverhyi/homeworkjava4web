package second.sorters;

public enum Direction {
    ASC(1), DESC(-1);

    private int direction;

    private Direction(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }
}

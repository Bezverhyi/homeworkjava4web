package second.performance;

public class Test {

    public static void main(String[] args) {
        char[] charArray = new char[400000];

        for (int i = 0; i < charArray.length; i++) {
            charArray[i] = (char) (Math.random() * 25 + (Math.random() > 0.5 ? 'A' : 'a'));
        }

        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder stringBuilder = new StringBuilder();
        String string = "";

        long firstTimeBuffer = System.currentTimeMillis();
        for (int i = 0; i < charArray.length; i++) {
            stringBuffer.append(charArray[i]);
        }
        long secondTimeBuffer = System.currentTimeMillis();

        long firstTimeBuilder = System.currentTimeMillis();
        for (int i = 0; i < charArray.length; i++) {
            stringBuilder.append(charArray[i]);
        }
        long secondTimeBuilder = System.currentTimeMillis();

        long firstTimeString = System.currentTimeMillis();
        for (int i = 0; i < charArray.length; i++) {
            string = string + charArray[i];
        }
        long secondTimeString = System.currentTimeMillis();

        System.out.println("StringBuffer: " + (secondTimeBuffer - firstTimeBuffer) + "ms");
        System.out.println("StringBuilder: " + (secondTimeBuilder - firstTimeBuilder) + "ms");
        System.out.println("String: " + (secondTimeString - firstTimeString) + "ms");
    }
}

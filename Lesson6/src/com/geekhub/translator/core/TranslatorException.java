package com.geekhub.translator.core;

public class TranslatorException extends Exception {

    public TranslatorException (Throwable e) {
        super(e);
    }
}

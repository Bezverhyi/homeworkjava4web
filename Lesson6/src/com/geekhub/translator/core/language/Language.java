package com.geekhub.translator.core.language;

public enum Language {

    ENGLISH("en"), UKRAINIAN("uk"), RUSSIAN("ru");

    private String code;

    Language(String code) {
        this.code = code;
    }

    public static Language find(String abbr) throws UnknownLanguageException {
        Language[] languages = Language.values();
        for (Language language : languages) {
            if (abbr.equals(language.getCode())) {
                return language;
            }
        }
        throw new UnknownLanguageException(abbr);
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }
}

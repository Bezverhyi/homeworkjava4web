package com.geekhub.translator.core.language;

public class LanguageDetectorException extends Exception {

    public LanguageDetectorException(Throwable e) {
        super(e);
    }
}

package com.geekhub.translator.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class IOUtils {

    private IOUtils() {
    }

    public static String toString(InputStream in) throws IOException {
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(in))) {
            StringBuilder loadedString = new StringBuilder();
            char[] buffer = new char[1024];
            int counter;

            while ((counter = inputStream.read(buffer)) != -1) {
                loadedString.append(buffer, 0, counter);
            }

            return loadedString.toString();
        }
    }
}

package com.geekhub.translator;

import com.geekhub.translator.core.language.Language;
import com.geekhub.translator.core.language.LanguageDetector;
import com.geekhub.translator.core.language.LanguageDetectorException;
import com.geekhub.translator.core.language.UnknownLanguageException;
import com.geekhub.translator.util.EncodingUtils;
import com.geekhub.translator.util.IOUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import org.json.JSONObject;

public class YandexLanguageDetector implements LanguageDetector {

    private static final String YANDEX_LANGUAGE_DETECTOR_API_URL = "https://translate.yandex.net/api/v1.5/tr.json/detect?key=%s&text=%s";

    private final String apiKey;

    public YandexLanguageDetector(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Language detect(String text) throws LanguageDetectorException {
        try {
            String langKey = parseResponse(IOUtils.toString(new URL(toUrl(text)).openStream()));

            return Language.find(langKey);
        } catch (IOException | UnknownLanguageException e) {
            throw new LanguageDetectorException(e);
        }
    }

    private String parseResponse(String response) {
        return new JSONObject(response).getString("lang");
    }

    private String toUrl(String text) throws UnsupportedEncodingException {
        String encodedText = EncodingUtils.encode(text, "UTF-8");
        return String.format(YANDEX_LANGUAGE_DETECTOR_API_URL, apiKey, encodedText);
    }
}

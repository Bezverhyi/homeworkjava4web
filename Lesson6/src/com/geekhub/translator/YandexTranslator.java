package com.geekhub.translator;

import com.geekhub.translator.core.Translation;
import com.geekhub.translator.core.TranslationRequest;
import com.geekhub.translator.core.Translator;
import com.geekhub.translator.core.TranslatorException;
import com.geekhub.translator.core.language.Language;
import com.geekhub.translator.core.language.LanguageDetector;
import com.geekhub.translator.core.language.LanguageDetectorException;
import com.geekhub.translator.util.EncodingUtils;
import com.geekhub.translator.util.IOUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

public class YandexTranslator implements Translator {

    private static final String YANDEX_TRANSLATOR_API_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s";

    private final String apiKey;
    private final LanguageDetector languageDetector;

    public YandexTranslator(String apiKey, LanguageDetector languageDetector) {
        this.apiKey = apiKey;
        this.languageDetector = languageDetector;
    }

    @Override
    public Translation translate(TranslationRequest translationRequest) throws TranslatorException {
        try {
            Language languageFrom = languageDetector.detect(translationRequest.getText());

            if (languageFrom != translationRequest.getTargetLanguage()) {
                String URL = toURL(translationRequest.getText(), translationRequest.getTargetLanguage(), languageFrom);
                String translatedText = parseResponse(IOUtils.toString(new URL(URL).openStream()));
                return new Translation(translationRequest.getText(), languageFrom, translatedText,
                                       translationRequest.getTargetLanguage());
            }

            return new Translation(translationRequest.getText(), languageFrom, translationRequest.getText(),
                                   translationRequest.getTargetLanguage());
        } catch (LanguageDetectorException | IOException e) {
            throw new TranslatorException(e);
        }
    }

    private String prepareLanguageDirection(Language from, Language to) {
        return from.getCode() + "-" + to.getCode();
    }

    private String parseResponse(String response){
        return new JSONObject(response).getJSONArray("text").getString(0);
    }

    private String toURL(String text, Language languageTo, Language languageFrom) throws UnsupportedEncodingException {
        String translateDirection = prepareLanguageDirection(languageFrom, languageTo);
        String encodedText = EncodingUtils.encode(text, "UTF-8");

        return String.format(YANDEX_TRANSLATOR_API_URL, apiKey, encodedText, translateDirection);
    }
}

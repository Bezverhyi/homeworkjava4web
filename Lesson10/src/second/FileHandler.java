package second;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileHandler {
    private String fileInput;
    private String fileOutput;

    public FileHandler(String fileInput, String fileOutput) {
        this.fileInput = fileInput;
        this.fileOutput = fileOutput;
    }

    public String[] getArrayOfUrl() throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fileInput))) {
            String buffer;
            List<String> urlList = new ArrayList<>();

            while ((buffer = reader.readLine()) != null) {
                urlList.add(buffer);
            }

            return urlList.toArray(new String[0]);
        }
    }

    public void write(String source) throws IOException {
        try (FileWriter writer = new FileWriter(fileOutput, true)) {
            writer.write(source + "\r\n");
        }
    }
}

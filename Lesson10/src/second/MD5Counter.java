package second;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Counter {
    private static final int WORD_A = 0x67452301;
    private static final int WORD_B = 0xEFCDAB89;
    private static final int WORD_C = 0x98BADCFE;
    private static final int WORD_D = 0x10325476;

    private static final int[] SHIFT_AMTS = {
            7, 12, 17, 22,
            5,  9, 14, 20,
            4, 11, 16, 23,
            6, 10, 15, 21
    };

    private static final int[] CONSTANT_TABLE = new  int[64];
    static {
        for (int i = 0; i < 64; i++) {
            CONSTANT_TABLE[i] = (int)(long)(4294967296L * Math.abs(Math.sin(i + 1)));
        }
    }

    public String getMD5(byte[] source) {
        int numberOfBlocks = (source.length + 8) / 64 + 1;
        int numberOfPaddingBytes = numberOfBlocks * 64 - source.length;

        long messageLenBits = (long)source.length * 8;
        byte[] paddingBytes = new byte[numberOfPaddingBytes];
        paddingBytes[0] = (byte)0x80;
        for (int i = 0; i < 8; i++) {
            paddingBytes[numberOfPaddingBytes - 8 + i] = (byte)messageLenBits;
            messageLenBits >>= 8;
        }

        byte[] preparedSource = appendArrays(source, paddingBytes);

        int a = WORD_A;
        int b = WORD_B;
        int c = WORD_C;
        int d = WORD_D;

        int[] buffer = new int[16];
        for (int i = 0; i < numberOfBlocks; i++) {
            int index = i * 64;
            for (int j = 0; j < 64; j++) {
                buffer[j / 4] = (buffer[j / 4] >> 8) + (preparedSource[index + j] << 24);
            }
            int originalA = a;
            int originalB = b;
            int originalC = c;
            int originalD = d;
            for (int j = 0; j < 64; j++) {
                int div16 = j / 16;
                int f = 0;
                int bufferIndex = j;
                switch (div16) {
                    case 0:
                        f = (b & c) | (~b & d);
                        break;
                    case 1:
                        f = (b & d) | (c & ~d);
                        bufferIndex = ((bufferIndex - 16) * 5 + 1) % 16;
                        break;
                    case 2:
                        f = b ^ c ^ d;
                        bufferIndex = ((bufferIndex - 32) * 3 + 5) % 16;
                        break;
                    case 3:
                        f = c ^ (b | ~d);
                        bufferIndex = ((bufferIndex - 32) * 7) % 16;
                        break;
                }
                int temp = b + Integer.rotateLeft(a + f + buffer[bufferIndex] + CONSTANT_TABLE[j], SHIFT_AMTS[div16 * 4 + j % 4]);
                System.out.println(temp);
                a = d;
                d = c;
                c = b;
                b = temp;
            }
            a += originalA;
            b += originalB;
            c += originalC;
            d += originalD;
        }

        int[] hash = {a, b, c, d};
        byte[] md5 = new byte[16];
        for (int i = 0; i < 16; i++) {
            md5[i] = (byte)hash[i / 4];
            hash[i / 4] >>= 8;
        }
        return toHexStringBuildIn(md5);
    }

    private byte[] appendArrays(byte[] firstArray, byte[] secondArray) {
        byte[] resultArray = new byte[firstArray.length + secondArray.length];

        for (int i = 0; i < firstArray.length; i++) {
            resultArray[i] = firstArray[i];
        }
        for (int i = 0; i < secondArray.length; i++) {
            resultArray[i + firstArray.length] = secondArray[i];
        }

        return resultArray;
    }

    public String getMD5BuildIn(byte[] source) {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            md.update(source);
            return toHexStringBuildIn(md.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String toHexStringBuildIn(byte[] b) {
        BigInteger bigInt = new BigInteger(1, b);
        return bigInt.toString(16);
    }
}

package second;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private final static String FILE_WITH_URL = "C:\\Users\\Dmytryi\\Downloads\\FileWithImages.txt";
    private final static String OUTPUT_FILE = "C:\\Users\\Dmytryi\\Downloads\\FileWithMD5.txt";

    public static void main(String[] args) throws IOException {
        FileHandler fileHandler = new FileHandler(FILE_WITH_URL, OUTPUT_FILE);

        String[] urlList = fileHandler.getArrayOfUrl();

        ExecutorService taskExecutor = Executors.newFixedThreadPool(10);

        for (String urlAddress : urlList) {
            Runnable task = new Task(urlAddress, fileHandler);
            taskExecutor.execute(task);
        }

        taskExecutor.shutdown();
    }
}
package second;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Task implements Runnable {

    private String urlAddress;
    private FileHandler fileHandler;
    private MD5Counter md5Counter = new MD5Counter();

    public Task(String urlAddress, FileHandler fileHandler) {
        this.urlAddress = urlAddress;
        this.fileHandler = fileHandler;
    }

    @Override
    public void run() {
        try (BufferedInputStream in = new BufferedInputStream(new URL(urlAddress).openStream())) {
            byte[] file = read(in);
            String md5BuildIn = md5Counter.getMD5BuildIn(file);
            String md5 = md5Counter.getMD5(file);
            fileHandler.write(md5BuildIn + " " + md5);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] read(BufferedInputStream in) throws IOException {
        try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;

            while ((length = in.read(buffer)) > 0) {
                result.write(buffer, 0, length);
            }

            return result.toByteArray();
        }
    }
}

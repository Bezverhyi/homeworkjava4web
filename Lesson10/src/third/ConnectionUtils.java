package third;

import java.io.*;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws java.io.IOException
     */
    public static byte[] getData(URL url) throws IOException {
        try (BufferedInputStream in = new BufferedInputStream(url.openStream());
             ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;

            while ((length = in.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            return output.toByteArray();
        }
    }

    public static String getPage(URL url) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
             CharArrayWriter writer = new CharArrayWriter()) {
            char[] buffer = new char[1024];
            int length;

            while ((length = reader.read(buffer)) > 0) {
                writer.write(buffer, 0, length);
            }

            return writer.toString();
        }
    }
}

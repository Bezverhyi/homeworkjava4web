package first;

public class StatementStorage {
    public static final String selectTop5CountryByRegion = "SELECT country.name, COUNT(region.country_id) AS regions " +
            "FROM country " +
            "LEFT JOIN region ON " +
            "country.id = region.country_id " +
            "GROUP BY country.name " +
            "ORDER BY regions DESC, country.name ASC " +
            "LIMIT 5";
    public static final String selectTop5CountryByCity = "SELECT country.name, COUNT(city.region_id) AS cities " +
            "FROM country " +
            "LEFT JOIN (region, city) ON " +
            "(country.id = region.country_id AND region.id = city.region_id) " +
            "GROUP BY country.name " +
            "ORDER BY cities DESC, country.name ASC " +
            "LIMIT 5";
    public static final String selectAllCountryWithRegionsAndCities = "SELECT country.name, COUNT(distinct region.id) " +
            "AS regions, COUNT(city.id) AS cities " +
            "FROM country " +
            "JOIN region ON " +
            "country.id = region.country_id " +
            "JOIN city ON " +
            "region.id = city.region_id " +
            "GROUP BY country.name " +
            "ORDER BY cities DESC, country.name ASC";
}
package first;

import java.sql.*;

public class Main {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String LOGIN = "root";
    private static final String PASSWORD = "crimson";
    private static final String DB_NAME = "geo";

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection(DB_URL + DB_NAME + "?useSSL=false", LOGIN, PASSWORD)) {
            Statement sqlStatement = connection.createStatement();
            ResultSet result;

            System.out.println("Select top 5 countries by regions number");
            result = sqlStatement.executeQuery(StatementStorage.selectTop5CountryByRegion);
            print(result);

            System.out.println("Select top 5 countries by cities number");
            result = sqlStatement.executeQuery(StatementStorage.selectTop5CountryByCity);
            print(result);

            System.out.println("Select all country with number of regions and cities");
            result = sqlStatement.executeQuery(StatementStorage.selectAllCountryWithRegionsAndCities);
            print(result);

        } catch (SQLException e) {
            System.out.println(e);;
        }
    }

    private static void print(ResultSet resultSet) throws SQLException {
        int numberOfColumns = resultSet.getMetaData().getColumnCount();
        int counter = 1;

        while (resultSet.next()) {
            System.out.print(counter + " ");
            for (int i = 1; i <= numberOfColumns; i++) {
                System.out.print(resultSet.getObject(i) + " ");
            }
            System.out.println("");
            counter++;
        }
    }
}

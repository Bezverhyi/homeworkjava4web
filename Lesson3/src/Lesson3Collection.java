import var_1.*;

import java.util.Scanner;

public class Lesson3Collection {
    private static final int PHONE_PRICE = 100;
    private static final int TABLET_PRICE = 200;
    private static final int NOTEBOOK_PRICE = 300;

    public static void main (String[] args){
        Inventory<Item> inventory = new Inventory<>();
        Scanner in = new Scanner(System.in);
        String input;
        System.out.println("Use this program you can put to inventory 3 different"
                + " kind of product: Phone, Tablet or Notebook,\nand calculate"
                + " total cost of the all goods, the total value of a particular"
                + " category or take a list of you inventory.\nTo add the product"
                + " please print \"add\" and choose kind of product.\nTo take a"
                + " cost of all goods please print \"all\".\nTo take total cost"
                + " of some value please choose type of product and type it.\n"
                + "And if you need list please type \"list\".\nFor exit type \"q\"");
        while (true) {
            input = in.next();

            if (input.equals("q")) {
                break;
            } else if (input.equals("add")) {
                System.out.print("Phone, Tablet, or Notebook.\nPlease choose: ");
                input = in.next().toUpperCase();
                switch (input) {
                    case "PHONE":
                        inventory.put(new Phone(PHONE_PRICE));
                        System.out.println("You add phone");
                        break;
                    case "TABLET":
                        inventory.put(new Tablet(TABLET_PRICE));
                        System.out.println("You add tablet");
                        break;
                    case "NOTEBOOK":
                        inventory.put(new Notebook(NOTEBOOK_PRICE));
                        System.out.println("You add notebook");
                        break;
                    default:
                        System.out.println("Sorry, you do not have added, you have"
                                + " to choose one of the suggested categories");
                        break;
                }
            } else if (input.equals("list")) {
                System.out.println(inventory.getList());

            } else if (input.equals("all")) {
                System.out.println("Sum of all items is: " + inventory.getAllItemsPrice());

            } else {
                switch (input.toUpperCase()) {
                    case "PHONE":
                        System.out.println(inventory.getItemPrice(new Phone(PHONE_PRICE)));
                        break;
                    case "TABLET":
                        System.out.println(inventory.getItemPrice(new Tablet(TABLET_PRICE)));
                        break;
                    case "NOTEBOOK":
                        System.out.println(inventory.getItemPrice(new Notebook(NOTEBOOK_PRICE)));
                        break;
                    default:
                        System.out.println("Please, select one of the options.");
                        break;
                }
            }
        }
    }
}

package var_1;

import java.util.*;

public class Inventory<T> {
    private ArrayList<T> items;

    public Inventory () {
        items = new ArrayList<>();
    }

    public void put(T item) {
        items.add(item);
    }

    public String getItemPrice(Item className) {
        int price = 0;
        int number = 0;
        for (T temp : items) {
            if (className.equals(temp)) {
                price = price + ((Item)temp).getPrice();
                number = number + 1;
            }
        }
        return (className.getClass().getName() + " " + number + " " + price);
    }

    public int getAllItemsPrice() {
        Collection<Integer> prices = getListPrice().values();
        Iterator iter = prices.iterator();
        int sum = 0;
        for (int price : prices) {
            sum = sum + price;
        }
        return sum;
    }

    public String getList() {
        HashMap<String, Integer> quantity = getListQuantity();
        HashMap<String, Integer> prices = getListPrice();
        Set<String> keys = quantity.keySet();
        String list = "";
        for (String key : keys) {
            list = list + key + " " + quantity.get(key) + " " + prices.get(key) + "\n";
        }
        return list;
    }

    private HashMap getListQuantity() {
        HashMap<String, Integer> className = new HashMap<>();
        for (T item : items) {
            String name = item.getClass().getName();
            if (!className.containsKey(name)) {
                className.put(name, 1);
            } else {
                className.put(name, className.get(name) + 1);
            }
        }
        return className;
    }

    private HashMap getListPrice() {
        HashMap<String, Integer> prices = new HashMap<>();
        for (T item : items) {
            String name = item.getClass().getName();
            if (!prices.containsKey(name)) {
                prices.put(name, ((Item)item).getPrice());
            } else {
                prices.put(name, prices.get(name) + ((Item)item).getPrice());
            }
        }
        return prices;
    }

}

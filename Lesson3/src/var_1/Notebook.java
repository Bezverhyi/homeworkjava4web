package var_1;

public class Notebook implements Item {
    private int price;

    public Notebook(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int newPrice){
        price = newPrice;
    }

    public int hashCode(){
        int result = 17;
        result = 31 * result + price;
        result = 31 * result + "Notebook".hashCode();
        return result;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Notebook temp = (Notebook) o;

        if (getPrice() != temp.getPrice()) {
            return false;
        }
        return true;
    }
}
package var_1;

public interface Item {

    int getPrice();

    void setPrice(int newPrice);

    int hashCode();

    boolean equals(Object o);
}


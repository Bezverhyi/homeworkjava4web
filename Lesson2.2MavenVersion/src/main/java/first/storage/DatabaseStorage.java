package first.storage;

import first.objects.Entity;
import first.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {

    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws StorageException {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws StorageException {
        String sqlStatement = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement selectStatement = connection.createStatement()) {
            ResultSet result = selectStatement.executeQuery(sqlStatement);
            return extractResult(clazz, result);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws StorageException {
        if (entity.isNew()) {
            return false;
        }
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setObject(1, entity.getId());

            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> int delete(Class<T> clazz) throws StorageException {
        String sqlStatement = "DELETE FROM " + clazz.getSimpleName() + " WHERE true";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws StorageException {
        Map<String, Object> entityMap = prepareEntity(entity);
        String sqlStatement;

        if (entity.isNew()) {
            sqlStatement = createInsertStatement(entityMap, entity.getClass().getSimpleName());
        } else {
            sqlStatement = createUpdateStatement(entityMap, entity.getClass().getSimpleName());
        }

        executeStatement(sqlStatement, entityMap, entity);
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws StorageException {
        Map<String, Object> result = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();

        try {
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    result.put(field.getName(), field.get(entity));
                }
            }
        } catch (IllegalAccessException e) {
            throw new StorageException(e);
        }

        return result;
    }

    private String createInsertStatement(Map<String, Object> entityMap, String tableName) {
        String names = "";
        String values = "";
        Set<String> fieldNames = entityMap.keySet();

        for (String fieldName : fieldNames) {
            names = names + fieldName + ",";
            values = values + "?,";
        }

        return String.format("INSERT INTO %s (%s) VALUES (%s)", tableName,
                             names.substring(0, names.length() - 1),
                             values.substring(0, values.length() - 1));
    }

    private String createUpdateStatement(Map<String, Object> entityMap, String tableName) {
        String set = "";
        Set<String> fieldNames = entityMap.keySet();
        for (String fieldName : fieldNames) {
            if (!fieldName.equals("id")) {
                set = set + fieldName + "=?,";
            }
        }
        return String.format("UPDATE %s SET %s WHERE id = ?", tableName, set.substring(0, set.length() - 1));
    }

    private <T extends Entity> void executeStatement(String sqlStatement, Map<String, Object> entityMap, T entity) throws StorageException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS
        )) {
            int counter = 1;
            Set<String> keySet = entityMap.keySet();
            for (String key : keySet) {
                preparedStatement.setObject(counter, entityMap.get(key));
                counter++;
            }
            if (!entity.isNew()) {
                preparedStatement.setObject(counter, entity.getId());
            }

            preparedStatement.execute();
            ResultSet generatedKey = preparedStatement.getGeneratedKeys();
            if (generatedKey.next()) {
                entity.setId(generatedKey.getInt(1));
            }
        } catch (SQLException e) {
            throw new StorageException(e);
        }
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> result = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();

        while (resultSet.next()) {
            T resultObject = clazz.newInstance();
            resultObject.setId(resultSet.getInt("id"));
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    field.set(resultObject, resultSet.getObject(field.getName()));
                }
            }
            result.add(resultObject);
        }

        return result;
    }
}

package first.com.geekhub;

import first.com.geekhub.source.SourceLoader;
import first.com.geekhub.source.SourceLoadingException;
import first.com.geekhub.source.URLSourceProvider;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException, TranslateException, ParserConfigurationException, SAXException {
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command;
        while(!"exit".equals(command = scanner.next())) {

            String source = null;
            try {
                source = sourceLoader.loadSource(command);
            } catch (SourceLoadingException e) {
                System.out.println(e);
            }
            String translation = translator.translate(source);

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);
        }
    }
}
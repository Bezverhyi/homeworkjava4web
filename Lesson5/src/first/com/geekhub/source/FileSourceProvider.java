package first.com.geekhub.source;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            return Files.exists(Paths.get(pathToSource));
        } catch (InvalidPathException e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws SourceLoadingException{
        try {
            return new String(Files.readAllBytes(Paths.get(pathToSource)), "UTF-8");
        } catch (IOException e) {
            throw new SourceLoadingException(e);
        }
    }
}

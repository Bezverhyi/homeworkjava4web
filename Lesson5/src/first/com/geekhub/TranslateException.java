package first.com.geekhub;

public class TranslateException extends Exception {
    public TranslateException(Throwable e) {
        super(e);
    }
}

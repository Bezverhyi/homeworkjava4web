package second;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class Parser {

    public static void parse(Path input) throws IOException {
        for (TypesOfArhives typesToArhive : TypesOfArhives.values()) {
            Files.walkFileTree(input, new SimpleFileVisitor<Path>() {
                public FileVisitResult visitFile(Path file,
                                                 BasicFileAttributes attrs)
                        throws IOException {
                    for (String type : typesToArhive.getTypesOfFile()) {
                        if (file.getFileName().toString().contains(type)) {
                            typesToArhive.addToFilesToArhive(file);
                            return FileVisitResult.CONTINUE;
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }
            });

            Arhivator.toArhive(input.getParent(), typesToArhive);
        }
    }
}

package second;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Path input = Paths.get(in.next()).toAbsolutePath();

        if (Files.exists(input)) {
            try {
                Parser.parse(input);
            } catch (IOException e) {
                System.out.println(e);
            }
        } else {
            System.out.println("Can't find: " + input);
        }
    }
}

package second;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Arhivator {

    public static void toArhive(Path outputDirectory, TypesOfArhives filesToArhive) throws IOException {
        if (filesToArhive.getFilesToArhive().isEmpty()) {
            return;
        }

        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(outputDirectory.resolve(filesToArhive.getNameOfZipFolder()).toFile()));

        for (Path fileToArhive : filesToArhive.getFilesToArhive()) {
            ZipEntry zipEntry = new ZipEntry(outputDirectory.relativize(fileToArhive).toString());
            zipOut.putNextEntry(zipEntry);
            zipOut.write(Files.readAllBytes(fileToArhive));
            zipOut.closeEntry();
        }

        zipOut.close();
    }
}

package second;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public enum TypesOfArhives {

    AUDIO("audio.zip", ".mp3", ".wav", ".wma"),
    VIDEO("video.zip", ".avi", ".mp4", ".flv"),
    IMAGE("image.zip", ".jpeg", ".jpg", ".gif", ".png");

    private String nameOfZipFolder;
    private String[] typesOfFile;
    private List<Path> filesToArhive = new LinkedList();

    private TypesOfArhives(String nameOfZipFolder, String... typesOfFile) {
        this.nameOfZipFolder = nameOfZipFolder;
        this.typesOfFile = typesOfFile;
    }

    public String getNameOfZipFolder() {
        return nameOfZipFolder;
    }

    public String[] getTypesOfFile() {
        return typesOfFile;
    }

    public void addToFilesToArhive(Path file) {
        filesToArhive.add(file);
    }

    public List<Path> getFilesToArhive() {
        return filesToArhive;
    }
}

package stream;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class CollectionUtils {

    private CollectionUtils() {
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> filteredList = new ArrayList<>();

        for (E element : elements) {
            if (filter.test(element)) {
                filteredList.add(element);
            }
        }

        return filteredList;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }

        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.size() == 0) {
            return false;
        }

        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }

        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return false;
            }
        }

        return true;
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> resultList = new ArrayList<>();

        for (T element : elements) {
            resultList.add(mappingFunction.apply(element));
        }

        return resultList;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }

        E maxElement = elements.get(0);

        for (E element : elements) {
            if (comparator.compare(maxElement, element) < 0) {
                maxElement = element;
            }
        }

        return Optional.of(maxElement);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return max(elements, comparator.reversed());
    }

    public static <E> List<E> distinct(List<E> elements) {
        List<E> resultList = new ArrayList<>();
        Set<E> controlSet = new HashSet<>();

        for (E element : elements) {
            if (controlSet.add(element)) {
                resultList.add(element);
            }
        }

        return resultList;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        } else if (elements.size() == 1) {
            return Optional.of(elements.get(0));
        }

        E finalElement = accumulator.apply(elements.get(0), elements.get(1));

        for (int i = 2; i < elements.size(); i++) {
            finalElement = accumulator.apply(finalElement, elements.get(i));
        }

        return Optional.of(finalElement);
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E finalElement = seed;

        for (E element : elements) {
            finalElement = accumulator.apply(finalElement, element);
        }

        return finalElement;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        Map<Boolean, List<E>> resultMap = new HashMap<>();

        resultMap.put(true, new ArrayList<>());
        resultMap.put(false, new ArrayList<>());

        for (E element : elements) {
            resultMap.get(predicate.test(element)).add(element);
        }

        return resultMap;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> resultMap = new HashMap<>();

        for (T element : elements) {
            resultMap.putIfAbsent(classifier.apply(element), new ArrayList<>());
            resultMap.get(classifier.apply(element)).add(element);
        }

        return resultMap;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction) {
        Map<K, U> resultMap = new HashMap<>();

        for (T element : elements) {
            resultMap.merge(keyFunction.apply(element), valueFunction.apply(element), mergeFunction);
        }

        return resultMap;
    }
}
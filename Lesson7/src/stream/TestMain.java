package stream;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

public class TestMain {

    public static void main(String[] args) {
        Person[] testClasses = {
                new Person("John", 12),
                new Person("Jane", 12),
                new Person("Jeniffer", 24),
                new Person("Jeniffer", 24),
                new Person("Smith", 35),
                new Person("Arnold", 15),
                new Person("Arnold", 25),
                new Person("Tony", 14),
                new Person("Tina", 10),
                new Person("Tina", 15),
                new Person("Tina", 15),
                new Person("Tina", 35),
                new Person("Valerian", 12),
                new Person("Valerian", 12),
        };

        List<Person> elements = Arrays.asList(testClasses);

        System.out.println(" filter: ");
        CollectionUtils.filter(elements, (e) -> e.getAge() > 12).stream().forEach(System.out::println);
        elements.stream().filter((e) -> e.getAge() > 12).forEach(System.out::println);

        System.out.println("\n anyMatch: ");
        System.out.println(CollectionUtils.anyMatch(elements, (e) -> e.getName().equals("Tina") && e.getAge() == 15));
        System.out.println(elements.stream().anyMatch((e) -> e.getName().equals("Tina") && e.getAge() == 15));

        System.out.println("\n allMatch: ");
        System.out.println(CollectionUtils.allMatch(elements, (e) -> e.getAge() > 3));
        System.out.println(elements.stream().allMatch((e) -> e.getAge() > 3));

        System.out.println("\n noneMatch: ");
        System.out.println(CollectionUtils.noneMatch(elements, (e) -> e.getAge() > 100));
        System.out.println(elements.stream().noneMatch((e) -> e.getAge() > 100));

        System.out.println("\n map: ");
        CollectionUtils.map(elements, Person::getName).stream().forEach(System.out::println);
        elements.stream().map(Person::getName).forEach(System.out::println);

        System.out.println("\n max: ");
        System.out.println(CollectionUtils.max(elements, (e1, e2) -> e1.getName().compareTo(e2.getName())));
        System.out.println(elements.stream().max((e1, e2) -> e1.getName().compareTo(e2.getName())));

        System.out.println("\n min: ");
        System.out.println(CollectionUtils.min(elements, Comparator.comparing(Person::getAge)));
        System.out.println(elements.stream().min(Comparator.comparing(Person::getAge)));

        System.out.println("\n distinct: ");
        CollectionUtils.distinct(elements).forEach(System.out::println);
        elements.stream().distinct().forEach(System.out::println);

        System.out.println("\n reduce: ");
        System.out.println(CollectionUtils
                                   .reduce(elements, (e1, e2) -> new Person(e1.getName() + e2.getName(),
                                                                            e1.getAge() + e2.getAge())));
        System.out.println(elements.stream()
                                   .reduce((e1, e2) -> new Person(e1.getName() + e2.getName(),
                                                                  e1.getAge() + e2.getAge())));

        System.out.println("\n reduce with seed: ");
        System.out.println(CollectionUtils
                                   .reduce(new Person("", 0), elements,
                                           (e1, e2) -> new Person(e1.getName() + e2.getName(),
                                                                  e1.getAge() + e2.getAge())));
        System.out.println(elements.stream()
                                   .reduce(new Person("", 0),
                                           (a, b) -> new Person(a.getName() + b.getName(),
                                                                a.getAge() + b.getAge())));

        System.out.println("\n partitionBy: ");
        System.out.println(CollectionUtils.partitionBy(elements, (e) -> e.getAge() > 20));
        System.out.println(elements.stream().collect(Collectors.partitioningBy((a) -> a.getAge() > 20)));

        System.out.println("\n groupBy: ");
        System.out.println(CollectionUtils.groupBy(elements, Person::getName));
        System.out.println(elements.stream().collect(Collectors.groupingBy(Person::getName)));
        System.out.println(elements.stream().collect(HashMap::new,
                                                     new BiConsumer<HashMap<String, List<Person>>, Person>() {

                                                         @Override
                                                         public void accept(HashMap<String, List<Person>> stringListHashMap, Person testClass) {
                                                             stringListHashMap.putIfAbsent(testClass.getName(), new ArrayList<Person>());
                                                             stringListHashMap.get(testClass.getName()).add(testClass);
                                                         }
                                                     },
                                                     new BiConsumer<HashMap<String, List<Person>>, HashMap<String, List<Person>>>() {

                                                         @Override
                                                         public void accept(HashMap<String, List<Person>> r, HashMap<String, List<Person>> r2) {
                                                             r.putAll(r2);
                                                         }
                                                     }));

        System.out.println("\n toMap: ");
        System.out.println(CollectionUtils.toMap(elements, Person::getAge, Person::getName, (a, b) -> a + ", " + b));
        System.out.println(elements.stream().collect(Collectors.toMap(Person::getAge, Person::getName, (a, b) -> a + ", " + b)));
        System.out.println(elements.stream().collect(HashMap<Integer, String>::new,
                                                     new BiConsumer<HashMap<Integer, String>, Person>() {
                                                         @Override
                                                         public void accept(HashMap<Integer, String> integerStringHashMap, Person testClass) {
                                                             integerStringHashMap.merge(testClass.getAge(), testClass.getName(), (a, b) -> a + ", " + b);
                                                         }
                                                     },
                                                     (a, b) -> a.putAll(b)));
    }
}

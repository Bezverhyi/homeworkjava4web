package list;

public class LinkedListImpl<E> implements LinkedList<E> {

    private int size = 0;

    private Node<E> tail;
    private Node<E> head;

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element, null);

        if (head == null) {
            head = newNode;
            tail = newNode;
            size++;
        } else {
            tail.next = newNode;
            tail = newNode;
            size++;
        }
    }

    @Override
    public E get(int index) {
        if (index > size - 1 || index < 0) {
            throw new IndexOutOfBoundsException();
        }

        return getNodeByIndex(index).value;
    }

    @Override
    public boolean contains(E element) {
        Node<E> pointer = head;

        for (int i = 0; i < size; i++) {
            if (pointer.value.equals(element)) {
                return true;
            } else {
                pointer = pointer.next;
            }
        }

        return false;
    }

    @Override
    public boolean delete(E element) {
        Node<E> pointer = head;
        Node<E> prev = head;

        for (int i = 0; i < size; i++) {
            if (pointer.value.equals(element)) {
                prev.next = pointer.next;
                size--;

                return true;
            }

            prev = pointer;
            pointer = pointer.next;
        }

        return false;
    }

    @Override
    public E delete(int index) {
        if (index > size - 1 || index < 0) {
            throw new IndexOutOfBoundsException();

        } else if (index == 0) {
            Node<E> pointer = head;
            head = head.next;
            size--;

            return pointer.value;
        } else {
            Node<E> prev = getNodeByIndex(index - 1);
            Node<E> pointer = prev.next;
            prev.next = pointer.next;
            size--;

            return pointer.value;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int clean() {
        int temp = size;

        tail = null;
        head = null;
        size = 0;

        return temp;
    }

    private Node<E> getNodeByIndex(int index) {
        Node<E> pointer = head;

        for (int i = 0; i < index; i++) {
            pointer = pointer.next;
        }

        return pointer;
    }
}
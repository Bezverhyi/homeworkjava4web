/*
 *
 */

/**
 * Circle class describes the properties and methods of the
 * circle for the second homework Java4Web course and
 * implements methods of interface Figure
 */
public class Circle implements Figure{
    private int radius;

    public Circle(int radius){
        this.radius = radius;
    }

    public double getPerimeter() {
        return (radius * 2 * Math.PI);
    }

    public double getArea() {
        return (radius * radius * Math.PI);
    }
}
/*
 *
 */

/**
 * Triangle class describes the properties and methods of the triangle
 * for the second homework Java4Web course and implements methods
 * of interface Figure
 */
public class Triangle implements Figure {
    private int sideA;
    private int sideB;
    private int sideC;

    public Triangle(int sideA, int sideB, int sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double getPerimeter() {
        return (sideA + sideB + sideC);
    }

    public double getArea() {
        double p = ((sideA + sideB + sideC) / 2);
        return (Math.cbrt(p * (p - sideA) * (p - sideB) * (p - sideC)));
    }
}
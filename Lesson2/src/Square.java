/*
 *
 */

/**
 * Square class describes the properties and methods of the
 * square for the second homework Java4Web course and
 * implements methods of interface Figure
 */
public class Square implements Figure{
    private int side;

    public Square(int side){
        this.side = side;
    }

    public double getPerimeter() {
        return (side * 4);
    }

    public double getArea() {
        return (side * side);
    }
}
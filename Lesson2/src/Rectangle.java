/*
 *
 */

/**
 * Rectangle class describes the properties and methods of the
 * rectangle for the second homework Java4Web course and
 * implements methods of interface Figure
 */
public class Rectangle implements Figure{
    private int sideA;
    private int sideB;

    public Rectangle(int sideLong, int sideShort){
        sideA = sideLong;
        sideB = sideShort;
    }

    public double getPerimeter() {
        return ((sideA + sideB) * 2);
    }

    public double getArea() {
        return (sideA * sideB);
    }
}

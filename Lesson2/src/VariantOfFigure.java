/*
 *
 */

/**
 * Enum class VariantOfFigure enumerate variants of figure that have
 * been implement for the second homework Java4Web course
 */
public enum VariantOfFigure {
    CIRCLE, SQUARE, RECTANGLE, TRIANGLE;
}
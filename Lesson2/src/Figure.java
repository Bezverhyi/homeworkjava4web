/*
 *
 */

/**
 *Figure interface was created as a common interface to the figures
 *which have been implement for the second homework Java4Web course.
 */
public interface Figure {
    double getArea();
    double getPerimeter();
}

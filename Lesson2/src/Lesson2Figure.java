/*
 *
 */

import java.util.Scanner;

/**
 *Second homework Java4Web course. There calculated area and perimeter
 * for one of the four implements figures by the user parameters.
 * Demonstrates basic properties of OOP
 */
public class Lesson2Figure {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        Figure figure = null;
        VariantOfFigure usersFigure;

        System.out.println("This program calculate a perimeter and area of Circle,"
                + " Rectangle, Square or Triangle");

        while (true) {
            try {
                System.out.print("Please specify what figure you need: ");
                String inner = in.next();
                usersFigure = Enum.valueOf(VariantOfFigure.class, inner.toUpperCase());
                break;
            } catch (IllegalArgumentException e) {
                System.out.println("Sorry, you enter invalid figure, try again");
            }
        }

        switch (usersFigure) {
            case CIRCLE:
                int radius;
                while (true) {
                    System.out.print("For Circle you must specify radius: ");
                    if (in.hasNextInt()) {
                        radius = in.nextInt();
                        break;
                    } else {
                        in.next();
                        System.out.println("Sorry, your enter is invalid, please,"
                                + " enter integer");
                    }
                }
                figure = new Circle(radius);
                break;

            case SQUARE:
                int side;
                while (true) {
                    System.out.print("For Square you must specify one of side: ");
                    if (in.hasNextInt()) {
                        side = in.nextInt();
                        break;
                    } else {
                        in.next();
                        System.out.println("Sorry, your enter is invalid, please,"
                                + " enter integer");
                    }
                }
                figure = new Square(side);
                break;

            case RECTANGLE:
                int sideA;
                int sideB;
                System.out.println("For Rectangle you must specify two side.");
                while (true) {
                    System.out.print("First side: ");
                    if (in.hasNextInt()) {
                        sideA = in.nextInt();
                        break;
                    } else {
                        in.next();
                        System.out.println("Sorry, your enter is invalid, please,"
                                + " enter integer");
                    }
                }
                while (true) {
                    System.out.print("Second side: ");
                    if (in.hasNextInt()) {
                        sideB = in.nextInt();
                        break;
                    } else {
                        in.next();
                        System.out.println("Sorry, your enter is invalid, "
                                + "please, enter integer");
                    }
                }
                figure = new Rectangle(sideA, sideB);
                break;

            case TRIANGLE:
                int a;
                int b;
                int c;
                boolean check = false;
                do {
                    System.out.println("For Triangle you must specify three side.");

                    while (true) {
                        System.out.print("First side: ");
                        if (in.hasNextInt()) {
                            a = in.nextInt();
                            break;
                        } else {
                            in.next();
                            System.out.println("Sorry, your enter is invalid,"
                                    + " please, enter integer");
                        }
                    }
                    while (true) {
                        System.out.print("Second side: ");
                        if (in.hasNextInt()) {
                            b = in.nextInt();
                            break;
                        } else {
                            in.next();
                            System.out.println("Sorry, your enter is invalid,"
                                    + " please, enter integer");
                        }
                    }
                    while (true) {
                        System.out.print("Third side: ");
                        if (in.hasNextInt()) {
                            c = in.nextInt();
                            break;
                        } else {
                            in.next();
                            System.out.println("Sorry, your enter is invalid,"
                                    + " please, enter integer");
                        }
                    }

                    // Check the properties of the triangle is closed
                    if ((a + b) < c) {
                        System.out.println("Sorry, Third side is too long,"
                                + " triangle is not closed");
                    } else if ((a + c) < b) {
                        System.out.println("Sorry, Second side is too long,"
                                + " triangle is not closed");
                    } else if ((b + c) < a) {
                        System.out.println("Sorry, First side is too long,"
                                + " triangle is not closed");
                    } else {
                        check = true;
                    }
                } while (check == false);

                figure = new Triangle(a, b, c);
                break;

            default:
                System.out.println("Default case");
        }

        if (figure != null) {
            System.out.printf("The area is: %.2f\n", figure.getArea());
            System.out.printf("The perimeter is: %.2f\n", figure.getPerimeter());
        }
    }

}

import org.omg.CORBA.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import source.FileSourceProvider;
import source.SourceLoader;
import source.URLSourceProvider;

/**
 * Created by Dmytryi on 28.01.2017.
 */

@Configuration
@PropertySource("translator.properties")
public class YandexTranslatorConfiguration {

    @Value("${translator.apiKey}")
    private String apiKey;
    @Value("${translator.translationDirection}")
    private String translationDirection;

    @Bean
    public FileSourceProvider fileSourceProvider() {
        return new FileSourceProvider();
    }

    @Bean
    public URLSourceProvider urlSourceProvider() {
        return new URLSourceProvider();
    }

    @Bean
    public SourceLoader sourceLoader() {
        return new SourceLoader(fileSourceProvider(), urlSourceProvider());
    }

    @Bean
    public Translator translator() {
        return new Translator(urlSourceProvider(), apiKey, translationDirection);
    }
}

package source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */

public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            URL check = new URL(pathToSource);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws SourceLoadingException {
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new URL(pathToSource).openStream()))) {
            StringBuilder loadedString = new StringBuilder();
            char[] buffer = new char[1024];
            int read;

            while ((read = inputStream.read(buffer)) != -1) {
                loadedString.append(buffer, 0, read);
            }

            return loadedString.toString();
        } catch (IOException e) {
            throw new SourceLoadingException(e);
        }
    }
}

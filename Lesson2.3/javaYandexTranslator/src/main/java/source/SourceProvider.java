package source;

/**
 * Base interface to access different sources.</br>
 * isAllowed method should protect and help to determine can we load resource for specified path ot not.
 */
public interface SourceProvider {

    boolean isAllowed(String pathToSource);

    String load(String pathToSource) throws SourceLoadingException;
}

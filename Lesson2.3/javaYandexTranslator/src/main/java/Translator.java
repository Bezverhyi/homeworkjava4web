import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import source.URLSourceProvider;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
 */

public class Translator {
    private final String yandexApiKey;
    private final String translationDirection;
    private final URLSourceProvider urlSourceProvider;

    public Translator(URLSourceProvider urlSourceProvider, String yandexApiKey, String translationDirection) {
        this.urlSourceProvider = urlSourceProvider;
        this.yandexApiKey = yandexApiKey;
        this.translationDirection = translationDirection;
    }

    public String translate(String original) throws TranslateException{
        try {
            return parseContent(urlSourceProvider.load(prepareURL(original)));
        } catch (Exception e) {
            throw new TranslateException(e);
        }
    }

    private String prepareURL(String text) throws IOException {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + yandexApiKey + "&text=" + encodeText(text) + "&lang=" + translationDirection;
    }

    private String parseContent(String content) throws ParserConfigurationException, IOException, SAXException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(content)));
        NodeList nList = document.getElementsByTagName("text");
        return nList.item(0).getTextContent();
    }

    private String encodeText(String text) throws IOException {
        return URLEncoder.encode(text, "UTF-8");
    }
}

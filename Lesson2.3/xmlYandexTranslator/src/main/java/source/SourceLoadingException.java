package source;

public class SourceLoadingException extends Exception {
    public SourceLoadingException(Throwable e) {
        super(e);
    }
}

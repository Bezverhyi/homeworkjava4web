package translate.source;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.annotation.Resources;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SourceLoader should contain all implementations of SourceProviders to be able to load different sources.
 */
@Component
public class SourceLoader {
    private List<SourceProvider> sourceProviders;

    @Autowired
    public SourceLoader(SourceProvider... sourceProviders) {
        this.sourceProviders = Arrays.asList(sourceProviders);

        System.out.println("Init SourceLoader");
        for (SourceProvider provider : sourceProviders) {
            System.out.println(provider);
        }
    }

    public String loadSource(String pathToSource) throws SourceLoadingException {
        for (SourceProvider sourceProvider : sourceProviders) {
            if (sourceProvider.isAllowed(pathToSource)) {
                return sourceProvider.load(pathToSource);
            }
        }
        return null;
    }
}

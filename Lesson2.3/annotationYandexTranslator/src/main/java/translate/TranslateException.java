package translate;

public class TranslateException extends Exception {
    public TranslateException(Throwable e) {
        super(e);
    }
}

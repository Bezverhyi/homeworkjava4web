package translate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.xml.sax.SAXException;
import translate.source.SourceLoader;
import translate.source.SourceLoadingException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException, TranslateException, ParserConfigurationException, SAXException {
        ApplicationContext context = new AnnotationConfigApplicationContext("translate");
        SourceLoader sourceLoader = context.getBean(SourceLoader.class);
        Translator translator = context.getBean(Translator.class);

        Scanner scanner = new Scanner(System.in);
        String command;
        while(!"exit".equals(command = scanner.next())) {

            String source = null;
            try {
                source = sourceLoader.loadSource(command);
            } catch (SourceLoadingException e) {
                System.out.println(e);
            }
            String translation = translator.translate(source);

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);
        }
    }
}
package third.profile;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProfileAnnotationWrapper {

    public <T> T wrap(T target) {
        Class<?> clazz = target.getClass();
        if (clazz.isAnnotationPresent(Profile.class)) {
            return doWrap(target);
        } else {
            return target;
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T doWrap(T target) {
        ClassLoader targetLoader = target.getClass().getClassLoader();
        Class[] targetInterfaces = target.getClass().getInterfaces();
        InvocationHandler handler = new MyHandler(target);

        return (T) Proxy.newProxyInstance(targetLoader, targetInterfaces, handler);
    }

    private void printer(Object[] args, Class[] parameters, long spendTime) {
        System.out.println();
        System.out.println("The number of parameters: " + args.length);

        for (int i = 0; i < args.length; i++) {
            System.out.println(parameters[i].getSimpleName() + " " + "args" + i + " = " + args[i]);
        }

        System.out.println("Spend time: " + spendTime);
        System.out.println();
    }

    private class MyHandler implements InvocationHandler {
        Object target;

        public MyHandler(Object target) {
            this.target = target;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            long beginTime = System.currentTimeMillis();
            Object result = method.invoke(target, args);
            long endTime = System.currentTimeMillis();

            args = (args == null ? new Object[0] : args);

            printer(args, method.getParameterTypes(), endTime - beginTime);

            return result;
        }
    }
}
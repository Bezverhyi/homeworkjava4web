package third.test;

public interface SimpleService {

    void action1();

    String action2();

    void sayHello(String name);

    void sayHello(String name, int times);
}

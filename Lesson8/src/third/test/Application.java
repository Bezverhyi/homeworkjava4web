package third.test;

import third.profile.ProfileAnnotationWrapper;

public class Application {

    public static void main(String[] args) {
        ProfileAnnotationWrapper wrapper = new ProfileAnnotationWrapper();

        SimpleService service1 = wrapper.wrap(new SimpleServiceImpl());
        service1.action1();
        System.out.println(service1.action2());
        service1.sayHello("Joe");
        service1.sayHello("Joe", 2);
    }
}

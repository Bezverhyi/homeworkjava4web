package third.test;

import third.profile.Profile;

@Profile
public class SimpleServiceImpl implements SimpleService {

    @Override
    public void action1() {
        System.out.println("action1");
    }

    @Override
    public String action2() {
        return "message";
    }

    @Override
    public void sayHello(String name) {
        System.out.println("Hello, " + name);
    }

    @Override
    public void sayHello(String name, int times) {
        for (int i = 0; i < times; i++) {
            System.out.println(name);
        }
    }
}

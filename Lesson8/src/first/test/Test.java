package first.test;

import first.json.JsonSerializer;

import java.awt.*;
import java.time.LocalDate;
import java.util.List;

public class Test {
    public static void main(String[] args) throws IllegalAccessException {
        Cat cat = new Cat();
        cat.setColor(Color.GRAY);
        cat.setAge(4);
        cat.setName("Tom");

        List<Integer> whiskers = cat.getWhiskers();
        whiskers.add(1);
        whiskers.add(2);
        whiskers.add(3);
        whiskers.add(4);
        whiskers.add(5);
        whiskers.add(6);

        Cat child1 = new Cat();
        child1.setColor(Color.GRAY);
        child1.setAge(4);
        child1.setName("Mikky");
        child1.setBirthDate(LocalDate.now());

        Cat child2 = new Cat();
        child2.setColor(Color.GRAY);
        child2.setAge(4);
        child2.setName("Mikky");
        child2.setBirthDate(LocalDate.now());

        cat.addChild(child1);
        cat.addChild(child2);

        cat.setBirthDate(LocalDate.now());
        cat.getPaws().put("front-left", new Paw(23, Color.GRAY));
        cat.getPaws().put("front-right", new Paw(24, Color.WHITE));
        cat.getPaws().put("back-left", new Paw(23, Color.BLACK));
        cat.getPaws().put("back-right", new Paw(22, Color.GRAY));

        System.out.println(JsonSerializer.serialize(cat));
    }
}

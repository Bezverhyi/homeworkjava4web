package first.json.adapter;

import first.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {

    @Override
    public Object toJson(Collection c) throws JSONException{
        String result = c.stream()
                         .map(e -> JsonSerializer.serialize(e))
                         .reduce("", (a, b) -> a + b.toString() + ",")
                         .toString();

        if (result.length() != 0) {
            result = result.substring(0, result.length() - 1);
        }

        return new JSONArray("[" + result + "]");
    }
}

package second.example;

public class Cat {
    private String color;
    private int age;
    private int legCount;
    @Ignore
    private int furLength;

    public Cat() {
    }

    public Cat(String color, int age, int legCount, int furLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.furLength = furLength;
    }

    public String getColor() {
        return color;
    }

    public int getAge() {
        return age;
    }

    public int getLegCount() {
        return legCount;
    }

    public int getFurLength() {
        return furLength;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "color='" + color + '\'' +
                ", age=" + age +
                ", legCount=" + legCount +
                ", furLength=" + furLength +
                '}';
    }
}

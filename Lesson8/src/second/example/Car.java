package second.example;

public class Car {
    private String color;
    private int maxSpeed;
    @Ignore
    private String type;
    private String model;

    public Car() {
    }

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getType() {
        return type;
    }

    public String getModel() {
        return model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}

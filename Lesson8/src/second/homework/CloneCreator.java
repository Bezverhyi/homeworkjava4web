package second.homework;

import java.lang.reflect.Field;

public class CloneCreator {

    public <E> E createClone(E element) throws IllegalAccessException, InstantiationException {
        E clone = (E) element.getClass().newInstance();
        Field[] fields = element.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            field.set(clone, field.get(element));
        }

        return clone;
    }
}

package second.homework;

import java.lang.reflect.Field;
import java.util.Arrays;

import second.example.Ignore;

public class BeanComparator {

    public <E> String compareInstanceFields(E o1, E o2) {
        Field[] fields = o1.getClass().getDeclaredFields();

        return Arrays.stream(fields)
                     .filter(e -> !e.isAnnotationPresent(Ignore.class))
                     .peek(e -> e.setAccessible(true))
                     .map(e -> toLine(e, o1, o2))
                     .reduce("", (a, b) -> (a + b));
    }

    private <E> String toLine(Field field, E o1, E o2) {
        try {
            return "\"" + field.getName() + "\""
                    + field.get(o1) + " " + field.get(o2) + " "
                    + field.get(o1).equals(field.get(o2)) + "\n";
        } catch (IllegalAccessException e) {
            System.out.println(e);
        }

        return "null";
    }
}

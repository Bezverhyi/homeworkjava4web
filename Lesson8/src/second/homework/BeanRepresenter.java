package second.homework;

import java.lang.reflect.Field;
import java.util.Arrays;

import second.example.Ignore;

public class BeanRepresenter {

    public <E> String represent(E element) {
        Field[] Fields = element.getClass().getDeclaredFields();

        return Arrays.stream(Fields)
                     .filter(e -> !e.isAnnotationPresent(Ignore.class))
                     .peek(e -> e.setAccessible(true))
                     .map(e -> toLine(e, element))
                     .reduce("", (a, b) -> a + b);
    }

    private<E> String toLine(Field field, E element) {
        try {
            return "\"" + field.getName() + "\"" + field.get(element) + "\n";
        } catch (IllegalAccessException e) {
            System.out.println(e);;
        }
        return "null";
    }
}

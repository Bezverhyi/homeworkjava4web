package second;

import second.example.Car;
import second.example.Cat;
import second.example.Human;
import second.homework.BeanComparator;
import second.homework.BeanRepresenter;
import second.homework.CloneCreator;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        Car car1 = new Car("red", 150, "hatchback", "Golf");
        Car car2 = new Car("black", 120, "sedan", "Volkswagen");

        Cat murka = new Cat("red", 2, 3, 90);
        Cat vaska = new Cat("white", 4, 4, 43);

        Human person1 = new Human(182, "male", 24, 80);
        Human person2 = new Human(165, "female", 18, 45);

        BeanRepresenter representer = new BeanRepresenter();
        BeanComparator comparator = new BeanComparator();
        CloneCreator cloneCreator = new CloneCreator();

        System.out.println(representer.represent(car1));
        System.out.println(representer.represent(car2));
        System.out.println(cloneCreator.createClone(car1));

        System.out.println(representer.represent(murka));
        System.out.println(representer.represent(vaska));
        System.out.println(cloneCreator.createClone(murka));

        System.out.println(representer.represent(person1));
        System.out.println(representer.represent(person2));
        System.out.println(cloneCreator.createClone(person1));

        System.out.println(comparator.compareInstanceFields(car1, car2));
        System.out.println(comparator.compareInstanceFields(car1, cloneCreator.createClone(car1)));

        System.out.println(comparator.compareInstanceFields(vaska, murka));
        System.out.println(comparator.compareInstanceFields(vaska, cloneCreator.createClone(vaska)));

        System.out.println(comparator.compareInstanceFields(person1, person2));
        System.out.println(comparator.compareInstanceFields(person1, cloneCreator.createClone(person1)));
    }
}

package first;

import java.time.LocalDateTime;

public class Task implements Comparable<Task> {
    private String name;
    private String category;
    private LocalDateTime time;

    public Task(String name, String category, LocalDateTime time) {
        this.name = name;
        this.category = category;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public int compareTo(Task o) {
        return time.compareTo(o.getTime());
    }
}
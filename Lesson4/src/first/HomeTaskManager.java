package first;

import java.time.LocalDateTime;
import java.util.*;

public class HomeTaskManager implements TaskManager {
    private Map<String, List<Task>> tasksByCategory = new HashMap<>();

    public void add(Task task) {
        if (tasksByCategory.containsKey(task.getCategory())) {
            tasksByCategory.get(task.getCategory()).add(task);
        } else {
            List<Task> tasks = new ArrayList<>();
            tasks.add(task);
            tasksByCategory.put(task.getCategory(), tasks);
        }
    }

    public void remove(LocalDateTime date) {
        for (String category : tasksByCategory.keySet()) {
            removeByCategory(date, category);
        }
    }

    public Set<String> getCategories() {
        return tasksByCategory.keySet();
    }

    public Map<String, List<Task>> getTasksByCategories(String... categories) {
        Map<String, List<Task>> tasksByCategories = new HashMap<>();

        for (String category : categories) {
            if (tasksByCategory.containsKey(category)) {
                tasksByCategories.put(category, getTasksByCategory(category));
            }
        }

        return tasksByCategories;
    }

    public List<Task> getTasksByCategory(String category) {
        if (tasksByCategory.containsKey(category)) {
            ArrayList<Task> tasks = new ArrayList<>();

            tasks.addAll(tasksByCategory.get(category));
            Collections.sort(tasks);

            return tasks;
        } else {
            return Collections.emptyList();
        }
    }

    public List<Task> getTasksForToday() {
        List<Task> tasksForToday = new ArrayList<>();
        LocalDateTime today = LocalDateTime.now();

        for (String category : tasksByCategory.keySet()) {
            for (Task task : tasksByCategory.get(category)) {
                if (task.getTime().getDayOfYear() == today.getDayOfYear()) {
                    tasksForToday.add(task);
                }
            }
        }

        Collections.sort(tasksForToday);
        return tasksForToday;
    }

    private void removeByCategory(LocalDateTime date, String category) {
        Iterator<Task> tasksCategoryIterator = tasksByCategory.get(category).iterator();

        while (tasksCategoryIterator.hasNext()) {
            if (tasksCategoryIterator.next().getTime().equals(date)) {
                tasksCategoryIterator.remove();
            }
        }
    }
}

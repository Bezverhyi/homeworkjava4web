package second;

public class WordUtils {
    private static final int MAX_WORD_LENGTH = 10;

    public static String shortify(String word) {
        if (word.length() > MAX_WORD_LENGTH) {
            return "" + word.charAt(0) + (word.length() - 2) + word.charAt(word.length() - 1);
        }
        return word;
    }
}

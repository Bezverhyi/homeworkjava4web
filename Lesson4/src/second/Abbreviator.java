package second;

import java.util.Scanner;

public class Abbreviator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.nextLine();

        System.out.println(WordUtils.shortify(word));
    }
}

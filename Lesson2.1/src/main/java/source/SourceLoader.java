package source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SourceLoader should contain all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader(SourceProvider... sourceProviders) {
        this.sourceProviders.addAll(Arrays.asList(sourceProviders));
    }

    public String loadSource(String pathToSource) throws SourceLoadingException {
        for (SourceProvider sourceProvider : sourceProviders) {
            if (sourceProvider.isAllowed(pathToSource)) {
                return sourceProvider.load(pathToSource);
            }
        }
        return "";
    }
}

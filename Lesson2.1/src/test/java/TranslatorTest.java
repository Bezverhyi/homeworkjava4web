import org.junit.Test;
import org.mockito.Mockito;
import source.URLSourceProvider;

import static org.junit.Assert.*;

public class TranslatorTest {
    private String request = "https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20161117T143319Z.440cfdaf2920197f.215ddb9801a78ea014f700fe8f5eb1a81c399733&text=text+for+translate&lang=ru";
    private String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<Translation code=\"200\" lang=\"en-ru\"><text>текст для перевода</text></Translation>";
    private String result = "текст для перевода";
    private String originalRequest = "text for translate";

    @Test
    public void translate() throws Exception {
        URLSourceProvider mockingUrlSourceProvider = Mockito.mock(URLSourceProvider.class);
        Mockito.when(mockingUrlSourceProvider.load(request)).thenReturn(response);
        Translator testedTranslator = new Translator(mockingUrlSourceProvider);
        assertEquals(result, testedTranslator.translate(originalRequest));
    }
}
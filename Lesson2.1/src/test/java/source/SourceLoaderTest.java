package source;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class SourceLoaderTest {
    private String testText = "test";
    private String pathToFile = "pathToFile";
    private String urlPath = "urlPath";

    @Test
    public void testLoadFromWrongPath() throws Exception {
        SourceLoader sourceLoader = new SourceLoader(new FileSourceProvider(), new URLSourceProvider());
        assertEquals("SourceLoader.loadSource(null)", "", sourceLoader.loadSource(null));
        assertEquals("SourceLoader.loadSource(\"\")", "", sourceLoader.loadSource(""));
        assertEquals("SourceLoader.loadSource(\"pathToWrongSource\")", "", sourceLoader.loadSource("pathToWrongSource"));
    }

    @Test
    public void testLoadFromFile() throws Exception {
        FileSourceProvider mockedFileSourceProvider = Mockito.mock(FileSourceProvider.class);

        Mockito.when(mockedFileSourceProvider.load(pathToFile)).thenReturn(testText);
        Mockito.when(mockedFileSourceProvider.isAllowed(pathToFile)).thenReturn(true);

        SourceLoader sourceLoader = new SourceLoader(mockedFileSourceProvider);

        assertEquals(testText, sourceLoader.loadSource(pathToFile));
    }

    @Test
    public void testLoadFromUrl() throws Exception {
        URLSourceProvider mockedUrlSourceProvider = Mockito.mock(URLSourceProvider.class);

        Mockito.when(mockedUrlSourceProvider.load(urlPath)).thenReturn(testText);
        Mockito.when(mockedUrlSourceProvider.isAllowed(urlPath)).thenReturn(true);

        SourceLoader sourceLoader = new SourceLoader(mockedUrlSourceProvider);

        assertEquals(testText, sourceLoader.loadSource(urlPath));
    }
}
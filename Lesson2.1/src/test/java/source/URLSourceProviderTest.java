package source;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class URLSourceProviderTest {
    URLSourceProvider testClass = new URLSourceProvider();
    private static String filePath = "tmp.txt";
    private static String fileText = "test";

    @BeforeClass
    public static void fileCreating() throws Exception {
        try (PrintWriter writer = new PrintWriter(filePath, "UTF-8")) {
            writer.print(fileText);
        }
    }

    @AfterClass
    public static void fileDeleting() throws IOException {
        Files.delete(Paths.get(filePath));
    }

    @Test
    public void testAllowUrlAddress() throws Exception {
        assertFalse("Test URLSourceProvider.isAllowed with null", testClass.isAllowed(null));
        assertFalse("Test URLSourceProvider.isAllowed with empty string", testClass.isAllowed(""));
        assertFalse("Test URLSourceProvider.isAllowed with wrong path", testClass.isAllowed("aaaaa"));
        assertTrue("Test URLSourceProvider.isAllowed with right path", testClass.isAllowed("https://docs.google.com/"));
    }

    @Test
    public void testLoadFromRightPath() throws Exception {
        Path absolutePath = Paths.get(filePath).toAbsolutePath();
        String urlPath = "file:///" + absolutePath;
        assertEquals(fileText, testClass.load(urlPath));
    }

    @Test (expected = SourceLoadingException.class)
    public void testLoadFromWrongPath() throws Exception {
        assertEquals(fileText, testClass.load(""));
    }

}
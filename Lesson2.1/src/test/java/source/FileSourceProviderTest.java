package source;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import static org.junit.Assert.*;

public class FileSourceProviderTest {
    private FileSourceProvider provider = new FileSourceProvider();
    private static String filePath = "tmp.txt";
    private static String fileText = "test";

    @BeforeClass
    public static void fileCreating() throws Exception {
        try (PrintWriter writer = new PrintWriter(filePath, "UTF-8")) {
            writer.print(fileText);
        }
    }

    @AfterClass
    public static void fileDeleting() throws IOException {
        Files.delete(Paths.get(filePath));
    }

    @Test
    public void testAllowFilePath() throws Exception{
        assertFalse("test FileSourceProvider.isAllowed() with null", provider.isAllowed(null));
        assertFalse("test FileSourceProvider.isAllowed() with empty string", provider.isAllowed(""));
        assertFalse("test FileSourceProvider.isAllowed() with wrong path", provider.isAllowed("aaaaa"));
        assertTrue("test FileSourceProvider.isAllowed() with right path", provider.isAllowed("tmp.txt"));
    }

    @Test
    public void testLoadFromRightPath() throws Exception {
        assertEquals(fileText, provider.load(filePath));
    }

    @Test (expected = SourceLoadingException.class)
    public void testLoadFromWrongPath() throws Exception {
        assertEquals(fileText, provider.load(""));
    }
}